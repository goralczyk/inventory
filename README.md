# About #

Inventory web application is meant to be a tool for creating catalogs and categories with dynamic fields to store various items.

### How do I get set up? ###

* run with maven: mvn tomcat7:run
* visit localhost:8080/inventory
* register or use default credentials: user@email.com / password

### Details ###

* This is a work in progress, most of the features aren't ready yet.
* Technologies: Java 8, Spring, Thymeleaf, H2DB, Bootstrap.

### Who do I talk to? ###

* Mail me at: goralczyq@gmail.com