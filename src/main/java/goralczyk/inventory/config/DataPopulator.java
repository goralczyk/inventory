package goralczyk.inventory.config;

import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.UserRepository;
import goralczyk.inventory.service.JsonService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * @author Tomasz Góralczyk
 */
@Profile("dev")
@Component
public class DataPopulator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JsonService jsonService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void populate() throws IOException {
        userRepository.save(new User("John Doe", "user@email.com", passwordEncoder.encode("password"), true));
        userRepository.save(new User("Empty User", "empty@email.com", passwordEncoder.encode("password"), true));
        jsonService.importCatalogs(IOUtils.toString(new ClassPathResource("data/john-doe.json").getInputStream(), "UTF-8"), 1);
    }
}
