package goralczyk.inventory.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Tomasz Góralczyk
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {}