package goralczyk.inventory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Tomasz Góralczyk
 */
public abstract class AbstractEntity {

    @JsonIgnore
    private long id;
    private String name;

    public AbstractEntity() {
        this(0, "");
    }

    public AbstractEntity(long id) {
        this(id, "");
    }

    public AbstractEntity(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractEntity that = (AbstractEntity) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
