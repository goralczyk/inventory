package goralczyk.inventory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public class Catalog extends AbstractEntity {

    private String tag;
    private String description;
    @JsonIgnore
    private long userId;
    private List<Category> rootCategories;

    public Catalog() {}

    public Catalog(long id) {
        super(id, "");
    }

    public Catalog(String tag, int userId) {
        this.tag = tag;
        this.userId = userId;
    }

    public Catalog(String name, String tag, String description, long userId) {
        this(0, name, tag, description, userId, new ArrayList<>());
    }

    public Catalog(long id, String name, String tag, String description, long userId) {
        this(id, name, tag, description, userId, new ArrayList<>());
    }

    public Catalog(long id, String name, String tag, String description, long userId, List<Category> rootCategories) {
        super(id, name);
        this.tag = tag;
        this.description = description;
        this.userId = userId;
        this.rootCategories = rootCategories;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<Category> getRootCategories() {
        return rootCategories;
    }

    public void setRootCategories(List<Category> rootCategories) {
        this.rootCategories = rootCategories;
    }
}
