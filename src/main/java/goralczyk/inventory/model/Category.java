package goralczyk.inventory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public class Category extends AbstractEntity {

    private String tag;
    private String description;
    @JsonIgnore
    private long catalogId;
    @JsonIgnore
    private Category parent;
    private List<Category> children;
    private List<Field> fields;
    private List<Item> items;

    // todo: rethink default constructors and equals/hashcode
    public Category() {}

    public Category(long id) {
        this(id, "", "", "", 0, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    public Category(String name, String tag, String description, long catalogId, Category parent) {
        this(0, name, tag, description, catalogId, parent, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    public Category(long id, String name, String tag, String description, long catalogId, Category parent) {
        this(id, name, tag, description, catalogId, parent, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    public Category(long id, String name, String tag, String description, long catalogId, Category parent, List<Category> children, List<Field> fields, List<Item> items) {
        super(id, name);
        this.tag = tag;
        this.description = description;
        this.catalogId = catalogId;
        this.parent = parent;
        this.children = children;
        this.fields = fields;
        this.items = items;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(long catalogId) {
        this.catalogId = catalogId;
    }

    @JsonIgnore
    public Long getParentId() {
        return parent != null ? parent.getId() : null;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Category clone() {
        return new Category(super.getId(), super.getName(), tag, description, catalogId, parent, children, fields, items);
    }
}
