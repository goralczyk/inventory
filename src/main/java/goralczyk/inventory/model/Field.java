package goralczyk.inventory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Tomasz Góralczyk
 */
public class Field extends AbstractEntity {

    private FieldType type;
    @JsonIgnore
    private Category category;

    public Field() {
    }

    public Field(String name, FieldType type, Category category) {
        this(0, name, type, category);
    }

    public Field(long id, String name, FieldType type, Category category) {
        super(id, name);
        this.type = type;
        this.category = category;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Field clone() {
        return new Field(super.getId(), super.getName(), type, category);
    }

    @Override
    public String toString() {
        return super.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Field field = (Field) o;
        return getName().equals(field.getName());

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getName().hashCode();
        return result;
    }
}
