package goralczyk.inventory.model;

import java.util.Date;

/**
 * @author Tomasz Góralczyk
 */
public enum FieldType {
    STRING(String.class, "String", 0),
    NUMBER(Number.class, "Number", 1),
    DATE(Date.class, "Date", 2);

    private Class type;
    private String typeName;
    private int typeCode;

    FieldType(Class type, String typeName, int typeCode) {
        this.typeName = typeName;
        this.type = type;
        this.typeCode = typeCode;
    }

    public static FieldType getByTypeCode(int typeCode) {
        switch (typeCode) {
            case 0:
                return STRING;
            case 1:
                return NUMBER;
            case 2:
                return DATE;
            default:
                throw new IllegalArgumentException(String.valueOf(typeCode));
        }
    }

    public String toString() {
        return typeName;
    }

    public Class getType() {
        return type;
    }

    public int getTypeCode() {
        return typeCode;
    }
}
