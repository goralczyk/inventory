package goralczyk.inventory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
public class Item extends AbstractEntity {

    private String description;
    private long quantity;

    @JsonIgnore
    private Category category;
    private Date timeCreated;

    @JsonDeserialize(keyUsing = MapKeyDeserializer.class)
    private Map<Field, Object> attributes;

    public Item() {
    }

    public Item(String name, String description, long quantity, Category category) {
        this(0, name, description, quantity, category, new Date(), new HashMap<>());
    }

    public Item(long id, String name, String description, long quantity, Category category, Date timeCreated) {
        this(id, name, description, quantity, category, timeCreated, new HashMap<>());
    }

    public Item(long id, String name, String description, long quantity, Category category, Date timeCreated, Map<Field, Object> attributes) {
        super(id, name);
        this.description = description;
        this.quantity = quantity;
        this.category = category;
        this.timeCreated = timeCreated;
        this.attributes = attributes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public Map<Field, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<Field, Object> attributes) {
        this.attributes = attributes;
    }

    public Item clone() {
        return new Item(super.getId(), super.getName(), description, quantity, category, timeCreated);
    }

    private static final class MapKeyDeserializer extends KeyDeserializer {

        @Override
        public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return new Field(key, null, null);
        }
    }
}
