package goralczyk.inventory.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public class User extends AbstractEntity {

    private String email;
    private String password;
    private boolean activated;
    private List<Catalog> catalogs;

    public User() {
        this(0, "", "", "", false, new ArrayList<>());
    }

    public User(String name, String email, String password, boolean activated) {
        this(0, name, email, password, activated, new ArrayList<>());
    }

    public User(long id, String name, String email, String password, boolean activated) {
        this(id, name, email, password, activated, new ArrayList<>());
    }

    public User(long id, String name, String email, String password, boolean activated, List<Catalog> catalogs) {
        super(id, name);
        this.email = email;
        this.password = password;
        this.activated = activated;
        this.catalogs = catalogs;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActivated() {
        return activated;
    }

    public Catalog getCatalogByTag(String tag) {
        return catalogs.stream()
                .filter(c -> c.getTag().equals(tag))
                .findFirst()
                .orElse(null);
    }

    public List<Catalog> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
