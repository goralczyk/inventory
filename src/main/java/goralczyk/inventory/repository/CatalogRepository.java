package goralczyk.inventory.repository;

import goralczyk.inventory.model.Catalog;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public interface CatalogRepository extends Repository<Catalog> {

    Catalog readByTagAndUserId(String tag, long userId);
    List<Catalog> readByUserId(long userId);
}
