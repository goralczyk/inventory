package goralczyk.inventory.repository;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;

import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public interface CategoryRepository extends Repository<Category> {

    /**
     * Returns list of categories, without fields, given a catalog and category path.
     * @param catalog catalog to process
     * @param path slash separated categories' tags
     * @return list of categories, without fields, in path's order
     */
    List<Category> readByPath(Catalog catalog, String path);

    /**
     * @param parentId
     * @return list of direct children, without fields.
     */
    List<Category> readByParentId(long parentId);

    /**
     * @param category
     * @return list of ancestor categories, without fields, in path's order
     */
    List<Category> readAncestors(Category category);

    /**
     * Returns list of subcategories without field data.
     * @param ancestorId base category id
     * @return list of descendant categories, without fields
     */
    List<Category> readDescendants(long ancestorId);

    /**
     * @param catalogId
     * @return list of categories with no parents under catalog, without fields
     */
    List<Category> readRootByCatalogId(long catalogId);

    /**
     * @param catalogId
     * @return list of categories under catalog, without fields
     */
    List<Category> readByCatalogId(long catalogId);

    long countByCatalogId(long catalogId);
}
