package goralczyk.inventory.repository;

import goralczyk.inventory.model.Field;

import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public interface FieldRepository extends Repository<Field> {

    List<Field> readByCategoryId(long categoryId);
}
