package goralczyk.inventory.repository;

import goralczyk.inventory.model.Field;

import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
public interface FieldValueRepository {

    void batchSave(long itemId, Map<Field, Object> attributes);
    Map<Field, Object> readByItemId(long itemId);
    void batchUpdate(long itemId, Map<Field, Object> attributes);
}
