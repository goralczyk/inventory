package goralczyk.inventory.repository;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Item;

import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
public interface ItemRepository extends Repository<Item> {

    /**
     * @param categoryId
     * @return list of items under category, without attributes
     */
    List<Item> readByCategoryId(long categoryId);

    /**
     * @param catalogId
     * @return list of items under catalog, without attributes or full category information
     */
    List<Item> readByCatalogId(long catalogId);

    /**
     * @param category
     * @param pageNumber
     * @param itemsPerPage
     * @return list of items sorted by name and id, without attributes
     */
    List<Item> readPaginatedByCategory(
            Category category,
            int pageNumber,
            int itemsPerPage);

    /**
     * @param catalogId
     * @param pageNumber
     * @param itemsPerPage
     * @return list of items sorted by name and id, without attributes
     */
    List<Item> readPaginatedByCatalogId(
            long catalogId,
            int pageNumber,
            int itemsPerPage);

    /**
     * @param categoryId - id of base category
     * @return number of items under base and descendant categories
     */
    long countByCategoryId(long categoryId);

    /**
     * @param catalogId
     * @return number of items under catalog
     */
    long countByCatalogId(long catalogId);
}
