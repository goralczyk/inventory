package goralczyk.inventory.repository;

import org.springframework.dao.DuplicateKeyException;

/**
 * @author Tomasz Góralczyk
 */
public interface Repository<T> {

    /**
     * Save record and return generated id.
     * @param t object to save
     * @return generated id
     * @throws DuplicateKeyException
     */
    long save(T t) throws DuplicateKeyException;
    T read(long id);
    void update(T t);
    void delete(long id);
}
