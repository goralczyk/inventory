package goralczyk.inventory.repository;

import goralczyk.inventory.model.User;
import org.springframework.dao.DuplicateKeyException;

/**
 * @author Tomasz Góralczyk
 */
public interface UserRepository extends Repository<User> {

    User readByEmail(String email);
}
