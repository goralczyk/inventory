package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.repository.CatalogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcCatalogRepository implements CatalogRepository {

    private static final String INSERT_CATALOG =
            "insert into Catalog (name, tag, description, user_id) values (?, ?, ?, ?)";

    private static final String SELECT_CATALOG_BY_ID =
            "select * from Catalog where id = ?";

    private static final String SELECT_CATALOGS_BY_USER_ID =
            "select * from Catalog where user_id = ? order by name";

    private static final String SELECT_CATALOG_BY_USER_ID_AND_TAG =
            "select * from Catalog where user_id = ? and tag = ?";

    private static final String UPDATE_CATALOG =
            "update Catalog set name = ?, tag = ?, description = ? where id = ?";

    private static final String DELETE_CATALOG =
            "delete from Catalog where id = ?";

    private final JdbcOperations jdbcOperations;

    @Autowired
    public JdbcCatalogRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public long save(Catalog catalog) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(INSERT_CATALOG, new String[] {"id"});
                    ps.setString(1, catalog.getName());
                    ps.setString(2, catalog.getTag());
                    ps.setString(3, catalog.getDescription());
                    ps.setLong(4, catalog.getUserId());
                    return ps;
                },
                keyHolder
        );
        return keyHolder.getKey().longValue();
    }

    @Override
    public Catalog read(long id) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_CATALOG_BY_ID,
                    new CatalogMapper(),
                    id
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Catalog readByTagAndUserId(String tag, long userId) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_CATALOG_BY_USER_ID_AND_TAG,
                    new CatalogMapper(),
                    userId,
                    tag);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * @return List of user's catalogs sorted by name
     * @param userId
     */
    @Override
    public List<Catalog> readByUserId(long userId) {
        return jdbcOperations.query(
                SELECT_CATALOGS_BY_USER_ID,
                new CatalogMapper(),
                userId
        );
    }

    @Override
    public void update(Catalog catalog) {
        jdbcOperations.update(
                UPDATE_CATALOG,
                catalog.getName(),
                catalog.getTag(),
                catalog.getDescription(),
                catalog.getId()
        );
    }

    @Override
    public void delete(long id) {
        jdbcOperations.update(DELETE_CATALOG, id);
    }

    private static final class CatalogMapper implements RowMapper<Catalog> {

        @Override
        public Catalog mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Catalog(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("tag"),
                    rs.getString("description"),
                    rs.getInt("user_id")
            );
        }
    }
}
