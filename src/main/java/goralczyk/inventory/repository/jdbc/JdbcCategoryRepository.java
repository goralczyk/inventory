package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;
import goralczyk.inventory.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcCategoryRepository implements CategoryRepository {

    private static final String INSERT_CATEGORY =
            "insert into Category (name, tag, description, catalog_id, parent_id) values (?, ?, ?, ?, ?)";

    private static final String SELECT_CATEGORY_BY_ID =
            "select * from Category where id = ?";

    private static final String SELECT_CATEGORIES_BY_PARENT_ID =
            "select * from Category where parent_id = ? order by name";

    private static final String SELECT_CATEGORIES_BY_CATALOG_ID =
            "select * from Category where catalog_id = ?";

    private static final String SELECT_ROOT_CATEGORIES =
            "select * from Category where catalog_id = ? and parent_id is null order by name";

    private static final String UPDATE_CATEGORY =
            "update Category set name = ?, tag = ?, description = ? where id = ?";

    private static final String DELETE_CATEGORY =
            "delete from Category where id = ?";

    private static final String COUNT_BY_CATALOG_ID =
            "select count(*) from Category where catalog_id = ?";

    private final JdbcOperations jdbcOperations;

    @Autowired
    public JdbcCategoryRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    /**
     * Saves category without fields.
     * @param category category to save
     * @throws DuplicateKeyException on tag and parentId duplication
     */
    @Override
    public long save(Category category) throws DuplicateKeyException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(INSERT_CATEGORY, new String[] {"id"});
                    ps.setString(1, category.getName());
                    ps.setString(2, category.getTag());
                    ps.setString(3, category.getDescription());
                    ps.setLong(4, category.getCatalogId());
                    ps.setObject(5, category.getParentId());
                    return ps;
                },
                keyHolder
        );
        return keyHolder.getKey().longValue();
    }

    /**
     * @param id category id
     * @return category without fields
     */
    @Override
    public Category read(long id) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_CATEGORY_BY_ID,
                    new CategoryMapper(),
                    id
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Category> readByPath(Catalog catalog, String path) {
        String[] tags = path.split("/");
        List<Category> tagMatchingCategories = readByTagsAndCatalogId(tags, catalog.getId());
        ArrayList<Category> pathCategories = new ArrayList<>(tags.length);
        if (tagMatchingCategories == null) {
            return pathCategories;
        }
        for (int i = 0; i < tags.length; ++i) {
            for (Category category : tagMatchingCategories) {
                if (tags[i].equals(category.getTag())) {
                    if ((i == 0 && category.getParent() == null) ||
                            (!pathCategories.isEmpty() && pathCategories.get(pathCategories.size() - 1).equals(category.getParent()))) {
                        pathCategories.add(category);
                        break;
                    }
                }
            }
        }
        return pathCategories;
    }

    /**
     * Reads all categories in catalog that match any of specified tags. Doesn't read fields.
     */
    private List<Category> readByTagsAndCatalogId(String[] tags, long catalogId) {
        String regEx = Arrays.stream(tags).collect(Collectors.joining("|"));
        return jdbcOperations.query(
                "select * from Category where catalog_id = ? and tag regexp ?",
                new CategoryMapper(),
                catalogId,
                regEx
        );
    }

    @Override
    public List<Category> readByParentId(long parentId) {
        return jdbcOperations.query(
                SELECT_CATEGORIES_BY_PARENT_ID,
                new CategoryMapper(),
                parentId
        );
    }

    @Override
    public List<Category> readAncestors(Category category) {
        List<Category> ancestors = new ArrayList<>();
        while (true) {
            if (category.getParentId() == null) {
                Collections.reverse(ancestors);
                return ancestors;
            }
            category = read(category.getParentId());
            ancestors.add(category);
        }
    }

    @Override
    public List<Category> readDescendants(long ancestorId) {
        List<Category> categories = jdbcOperations.query(
                SELECT_CATEGORIES_BY_PARENT_ID,
                new CategoryMapper(),
                ancestorId
        );
        for (Category category : new ArrayList<>(categories)) {
            categories.addAll(readDescendants(category.getId()));
        }
        return categories;
    }

    @Override
    public List<Category> readRootByCatalogId(long catalogId) {
        return jdbcOperations.query(
                SELECT_ROOT_CATEGORIES,
                new CategoryMapper(),
                catalogId
        );
    }

    @Override
    public List<Category> readByCatalogId(long catalogId) {
        return jdbcOperations.query(
                SELECT_CATEGORIES_BY_CATALOG_ID,
                new CategoryMapper(),
                catalogId
        );
    }

    @Override
    public long countByCatalogId(long catalogId) {
        return jdbcOperations.queryForObject(
                COUNT_BY_CATALOG_ID,
                Integer.class,
                catalogId
        );
    }

    @Override
    public void update(Category category) {
        jdbcOperations.update(
                UPDATE_CATEGORY,
                category.getName(),
                category.getTag(),
                category.getDescription(),
                category.getId()
        );
    }

    @Override
    public void delete(long id) {
        jdbcOperations.update(
                DELETE_CATEGORY,
                id
        );
    }

    private static class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Long parentId = (Long) rs.getObject("parent_id");
            return new Category(
                    rs.getLong("id"),
                    rs.getString("name"),
                    rs.getString("tag"),
                    rs.getString("description"),
                    rs.getInt("catalog_id"),
                    parentId != null ? new Category(parentId) : null
            );
        }
    }
}
