package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import goralczyk.inventory.repository.CategoryRepository;
import goralczyk.inventory.repository.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcFieldRepository implements FieldRepository {

    private static final String INSERT =
            "insert into Field (type, name, category_id) values (?, ?, ?)";

    private static final String SELECT_BY_ID =
            "select * from Field where id = ?";

    private static final String SELECT_BY_CATEGORY_ID =
            "select * from Field where category_id = ?";

    private static final String UPDATE =
            "update Field set name = ? where id = ?";

    private static final String DELETE =
            "delete from Field where id = ?";

    private final JdbcOperations jdbcOperations;
    private final CategoryRepository categoryRepository;

    @Autowired
    public JdbcFieldRepository(JdbcOperations jdbcOperations, CategoryRepository categoryRepository) {
        this.jdbcOperations = jdbcOperations;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public long save(Field field) throws DuplicateKeyException {
        try {
            assertNameUnique(field);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcOperations.update(
                    con -> {
                        PreparedStatement ps = con.prepareStatement(INSERT, new String[] {"id"});
                        ps.setInt(1, field.getType().getTypeCode());
                        ps.setString(2, field.getName());
                        ps.setLong(3, field.getCategory().getId());
                        return ps;
                    },
                    keyHolder
            );
            return keyHolder.getKey().longValue();
        } catch (DuplicateKeyException e) {
            throw new DuplicateKeyException("Field \"" + field.getName() + "\" exists.");
        }
    }

    /**
     * Asserts field name is unique among ancestor and descendant categories' fields.
     * Category level assertion handled by DB.
     *
     * @param field
     * @throws DuplicateKeyException
     */
    private void assertNameUnique(Field field) throws DuplicateKeyException {
        // assert Parent is set
        field.setCategory(categoryRepository.read(field.getCategory().getId()));
        List<Category> categories = categoryRepository.readAncestors(field.getCategory());
        categories.addAll(categoryRepository.readDescendants(field.getCategory().getId()));
        for (Category c : categories) {
            List<Field> fields = readByCategoryId(c.getId());
            for (Field f : fields) {
                if (f.getName().equals(field.getName())) {
                    throw new DuplicateKeyException("Field \"" + f.getName() + "\" exists.");
                }
            }
        }
    }

    @Override
    public Field read(long id) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_BY_ID,
                    new FieldMapper(),
                    id
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Field> readByCategoryId(long categoryId) {
        return jdbcOperations.query(
                SELECT_BY_CATEGORY_ID,
                new FieldMapper(),
                categoryId
        );
    }

    @Override
    public void update(Field field) throws DuplicateKeyException {
        try {
            assertNameUnique(field);
            jdbcOperations.update(
                    UPDATE,
                    field.getName(),
                    field.getId()
            );
        } catch (DuplicateKeyException e) {
            throw new DuplicateKeyException("Field \"" + field.getName() + "\" exists.");
        }
    }

    @Override
    public void delete(long id) {
        jdbcOperations.update(DELETE, id);
    }

    private static class FieldMapper implements RowMapper<Field> {

        @Override
        public Field mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Field(
                    rs.getLong("id"),
                    rs.getString("name"),
                    FieldType.getByTypeCode(rs.getByte("type")),
                    new Category(rs.getLong("category_id"))
            );
        }
    }
}
