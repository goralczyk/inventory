package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import goralczyk.inventory.repository.FieldValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcFieldValueRepository implements FieldValueRepository {

    // todo: refactor
    private static final String INSERT_VALUE =
            //"insert into ??? (value, item_id, field_id) values (?, ?, ?)";
            "merge into ??? (value, item_id, field_id) values (?, ?, ?)";

    private static final String SELECT_VALUES =
            "select * from ??? join Field on field_id = Field.id where item_id = ? order by Field.name";

    private static final String UPDATE_VALUE =
            "update ??? set value = ? where item_id = ? and field_id = ?";

    private static final String[] TABLE_NAMES;

    private static final Map<FieldType, String> INSERT_MAP;
    private static final Map<FieldType, String> SELECT_MAP;
    private static final Map<FieldType, String> UPDATE_MAP;

    static {
        TABLE_NAMES = new String[FieldType.values().length];
        for (int i = 0; i < TABLE_NAMES.length; ++i) {
            TABLE_NAMES[i] = "FieldValue" + FieldType.getByTypeCode(i);
        }
        INSERT_MAP = new LinkedHashMap<>();
        SELECT_MAP = new LinkedHashMap<>();
        UPDATE_MAP = new LinkedHashMap<>();
        for (int i = 0; i < TABLE_NAMES.length; ++i) {
            String tableName = TABLE_NAMES[i];
            INSERT_MAP.put(FieldType.getByTypeCode(i), INSERT_VALUE.replace("???", tableName));
            SELECT_MAP.put(FieldType.getByTypeCode(i), SELECT_VALUES.replace("???", tableName));
            UPDATE_MAP.put(FieldType.getByTypeCode(i), UPDATE_VALUE.replace("???", tableName));
        }
    }

    private final JdbcOperations jdbcOperations;

    @Autowired
    public JdbcFieldValueRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }


    @Override
    public void batchSave(long itemId, Map<Field, Object> attributes) {
        attributes.forEach((field, value) -> jdbcOperations.update(
                INSERT_MAP.get(field.getType()),
                String.valueOf(value).length() > 0 ? value : null,
                itemId,
                field.getId()
        ));
    }

    @Override
    public Map<Field, Object> readByItemId(long itemId) {
        Map<Field, Object> attributes = new LinkedHashMap<>();
        SELECT_MAP.values().forEach(sql ->
            jdbcOperations.query(
                    sql,
                    new FieldValueMapper(),
                    itemId
            ).forEach(e -> attributes.put(e.getKey(), e.getValue()))
        );
        return attributes;
    }

    @Override
    public void batchUpdate(long itemId, Map<Field, Object> attributes) {
        attributes.forEach((field, value) -> jdbcOperations.update(
                UPDATE_MAP.get(field.getType()),
                String.valueOf(value).length() > 0 ? value : null,
                itemId,
                field.getId()
        ));
    }

    private static final class FieldValueMapper implements RowMapper<Map.Entry<Field, Object>> {

        @Override
        public Map.Entry<Field, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
            Field field = new Field(
                    rs.getLong("Field.id"),
                    rs.getString("Field.name"),
                    FieldType.getByTypeCode(rs.getByte("Field.type")),
                    new Category(rs.getLong("Field.category_id"))
            );
            return new AbstractMap.SimpleEntry<>(
                    field,
                    rs.getObject("value")
            );
        }
    }
}
