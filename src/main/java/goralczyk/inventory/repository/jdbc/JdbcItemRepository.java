package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Item;
import goralczyk.inventory.repository.CategoryRepository;
import goralczyk.inventory.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcItemRepository implements ItemRepository {

    private static final String INSERT_ITEM =
            "insert into Item (name, description, quantity, category_id) values (?, ?, ?, ?)";

    private static final String SELECT_ITEM_BY_ID =
            "select * from Item where id = ?";

    private static final String SELECT_ITEM_BY_CATEGORY_ID =
            "select * from Item where category_id = ?";

    private static final String SELECT_ITEM_BY_CATALOG_ID =
            "select * from Item where (select catalog_id from Category where Category.id = Item.category_id) = ?";

    private static final String SELECT_PAGINATED_BY_CATEGORY_ID =
            "select * from Item where category_id in ??? order by name, id limit ? offset ?";

    private static final String UPDATE_ITEM =
            "update Item set name = ?, description = ?, quantity = ? where id = ?";

    private static final String DELETE_ITEM =
            "delete from Item where id = ?";

    private static final String COUNT_BY_CATEGORY_ID =
            "select count(*) from Item where category_id in ???";

    private static final String COUNT_BY_CATALOG_ID =
            "select count(*) from Item where (select catalog_id from Category where Category.id = Item.category_id) = ?";

    private final JdbcOperations jdbcOperations;

    private final CategoryRepository categoryRepository;

    @Autowired
    public JdbcItemRepository(JdbcOperations jdbcOperations, CategoryRepository categoryRepository) {
        this.jdbcOperations = jdbcOperations;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public long save(Item item) throws DuplicateKeyException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(INSERT_ITEM, new String[] {"id"});
                    ps.setString(1, item.getName());
                    ps.setString(2, item.getDescription());
                    ps.setLong(3, item.getQuantity());
                    ps.setLong(4, item.getCategory().getId());
                    return ps;
                },
                keyHolder
        );
        return keyHolder.getKey().longValue();
    }

    @Override
    public Item read(long id) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_ITEM_BY_ID,
                    new ItemMapper(),
                    id
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Item> readByCategoryId(long categoryId) {
        return jdbcOperations.query(
                SELECT_ITEM_BY_CATEGORY_ID,
                new ItemMapper(),
                categoryId
        );
    }

    @Override
    public List<Item> readByCatalogId(long catalogId) {
        return jdbcOperations.query(
                SELECT_ITEM_BY_CATALOG_ID,
                new ItemMapper(),
                catalogId
        );
    }

    @Override
    public List<Item> readPaginatedByCategory(Category category, int pageNumber, int itemsPerPage) {
        List<Category> categories = categoryRepository.readDescendants(category.getId());
        categories.add(category);
        return jdbcOperations.query(
                SELECT_PAGINATED_BY_CATEGORY_ID
                        .replace("???", "(" +
                                categories.stream().map(c ->
                                        String.valueOf(c.getId())).sorted()
                                        .collect(Collectors.joining(",")) +
                                ")"),
                new ItemMapper(),
                itemsPerPage,
                (pageNumber - 1) * itemsPerPage
        );
    }

    @Override
    public List<Item> readPaginatedByCatalogId(long catalogId, int pageNumber, int itemsPerPage) {
        List<Category> categories = categoryRepository.readByCatalogId(catalogId);
        return jdbcOperations.query(
                SELECT_PAGINATED_BY_CATEGORY_ID
                        .replace("???", "(" +
                                categories.stream().map(c ->
                                        String.valueOf(c.getId())).sorted()
                                        .collect(Collectors.joining(",")) +
                                ")"),
                new ItemMapper(),
                itemsPerPage,
                (pageNumber - 1) * itemsPerPage
        );
    }

    @Override
    public void update(Item item) {
        jdbcOperations.update(
                UPDATE_ITEM,
                item.getName(),
                item.getDescription(),
                item.getQuantity(),
                item.getId()
        );
    }

    @Override
    public void delete(long id) {
        jdbcOperations.update(DELETE_ITEM, id);
    }

    @Override
    public long countByCategoryId(long categoryId) {
        List<Category> categories = categoryRepository.readDescendants(categoryId);
        categories.add(categoryRepository.read(categoryId));
        return jdbcOperations.queryForObject(
                COUNT_BY_CATEGORY_ID.replace("???", "(" + categories.stream().map(c -> String.valueOf(c.getId())).collect(Collectors.joining(",")) + ")"),
                Integer.class
        );
    }

    @Override
    public long countByCatalogId(long catalogId) {
        return jdbcOperations.queryForObject(
                COUNT_BY_CATALOG_ID,
                Integer.class,
                catalogId
        );
    }

    private static class ItemMapper implements RowMapper<Item> {

        @Override
        public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Item(
                    rs.getLong("id"),
                    rs.getString("name"),
                    rs.getString("description"),
                    rs.getLong("quantity"),
                    new Category(rs.getLong("category_id")),
                    rs.getDate("time_created")
            );
        }
    }
}
