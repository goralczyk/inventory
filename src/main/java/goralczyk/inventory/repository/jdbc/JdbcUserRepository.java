package goralczyk.inventory.repository.jdbc;

import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Tomasz Góralczyk
 */
@Repository
public class JdbcUserRepository implements UserRepository {

    private static final String INSERT_USER =
            "insert into User (name, email, password, activated) values (?, ?, ?, ?)";

    private static final String SELECT_USER_BY_EMAIL =
            "select id, name, email, password, activated from User where email = ?";

    private static final String SELECT_USER_BY_ID =
            "select id, name, email, password, activated from User where id = ?";

    private static final String UPDATE_USER =
            "update User set name = ?, password = ? where id = ?";

    private static final String DELETE_USER =
            "delete from User where id = ?";

    private final JdbcOperations jdbcOperations;

    @Autowired
    public JdbcUserRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public long save(User user) throws DuplicateKeyException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(
                con -> {
                    PreparedStatement ps = con.prepareStatement(INSERT_USER, new String[] {"id"});
                    ps.setString(1, user.getName());
                    ps.setString(2, user.getEmail());
                    ps.setString(3, user.getPassword());
                    ps.setBoolean(4, user.isActivated());
                    return ps;
                },
                keyHolder
        );
        return keyHolder.getKey().longValue();
    }

    @Override
    public User read(long id) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_USER_BY_ID,
                    new UserMapper(),
                    id
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public User readByEmail(String email) {
        try {
            return jdbcOperations.queryForObject(
                    SELECT_USER_BY_EMAIL,
                    new UserMapper(),
                    email
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void update(User user) {
        jdbcOperations.update(
                UPDATE_USER,
                user.getName(),
                user.getPassword(),
                user.getId()
        );
    }

    @Override
    public void delete(long id) {
        jdbcOperations.update(DELETE_USER, id);
    }

    private static final class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getBoolean("activated")
            );
        }
    }
}
