package goralczyk.inventory.service;

import goralczyk.inventory.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Extension of basic UserDetails implementing class.
 * Enables hitting DB with user's id instead of email.
 *
 * @author Tomasz Góralczyk
 */
public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

    private final int id;

    public CustomUserDetails(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getEmail(), user.getPassword(), authorities);
        id = (int) user.getId();
    }

    public int getId() {
        return id;
    }
}
