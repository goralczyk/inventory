package goralczyk.inventory.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import goralczyk.inventory.model.*;
import goralczyk.inventory.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Service
public class JsonService {

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Reads all catalog data and returns JSON String.
     */
    public String exportCatalogs(List<Catalog> catalogs) throws JsonProcessingException {
        catalogs.forEach(this::readFullCatalogData);
        return objectMapper.writeValueAsString(catalogs);
    }

    /**
     * Reads data from DB and creates JSON with catalogs and categories names and ids.
     */
    public String exportCatalogsRaw(List<Catalog> catalogs) throws JsonProcessingException {
        List<RawNode> cat = catalogs.stream().map(this::readRawCatalogData).collect(Collectors.toList());
        return objectMapper.writeValueAsString(cat);
    }

    /**
     *  Reads data from DB and creates JSON appropriate for bootstrap-treeview
     */
    public String exportCatalogTreeView(Catalog catalog) throws JsonProcessingException {
        List<TreeViewNode> nodes = new ArrayList<>();
        readFullCatalogData(catalog);
        for (Category category : catalog.getRootCategories()) {
            nodes.add(TreeViewNode.categoryToNode(category));
        }
        return objectMapper.writeValueAsString(nodes);
    }

    private static final class RawNode {
        private long id;
        private String name;
        private List<RawNode> children;

        public RawNode(AbstractEntity entity) {
            this.id = entity.getId();
            this.name = entity.getName();
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setChildren(List<RawNode> children) {
            this.children = children;
        }

        public List<RawNode> getChildren() {
            return children;
        }
    }

    private static final class TreeViewNode {
        private String text;
        private String type;
        private String href;
        private String[] tags;
        private List<TreeViewNode> nodes;

        public static TreeViewNode categoryToNode(Category category) {
            TreeViewNode node = new TreeViewNode(category);
            for (Category child : category.getChildren()) {
                node.getNodes().add(categoryToNode(child));
            }
            for (Item item : category.getItems()) {
                node.getNodes().add(new TreeViewNode(item));
            }
            return node;
        }

        public TreeViewNode(AbstractEntity abstractEntity) {
            this.text = abstractEntity.getName();
            this.type = abstractEntity.getClass().getSimpleName().toLowerCase();
            this.href = type + "/view?id=" + abstractEntity.getId();
            if (abstractEntity instanceof Item) {
                this.tags = new String[]{String.valueOf(((Item) abstractEntity).getQuantity())};
            } else if (abstractEntity instanceof Category) {
                nodes = new ArrayList<>();
            }
        }

        public String getText() {
            return text;
        }

        public String getType() {
            return type;
        }

        public String getHref() {
            return href;
        }

        public String[] getTags() {
            return tags;
        }

        public List<TreeViewNode> getNodes() {
            return nodes;
        }
    }

    /**
     * Saves in DB and returns list of catalogs from JSON String.
     */
    public List<Catalog> importCatalogs(String json, long userId) throws IOException {
        List<Catalog> catalogs = objectMapper.readValue(json, new TypeReference<List<Catalog>>() {
        });
        catalogs.forEach(c -> c.setUserId(userId));
        catalogs.forEach(this::saveFullCatalogData);
        return catalogs;
    }

    public void readFullCatalogData(Catalog catalog) {
        catalog.setRootCategories(categoryRepository.readRootByCatalogId(catalog.getId()));
        catalog.getRootCategories().forEach(this::setCategoryDetails);
    }

    public RawNode readRawCatalogData(Catalog catalog) {
        catalog.setRootCategories(categoryRepository.readRootByCatalogId(catalog.getId()));
        RawNode rawNode = new RawNode(catalog);
        rawNode.setChildren(catalog.getRootCategories().stream().map(this::setRawCategoryDetails).collect(Collectors.toList()));
        return rawNode;
    }

    private RawNode setRawCategoryDetails(Category category) {
        category.setChildren(categoryRepository.readByParentId(category.getId()));
        RawNode rawNode = new RawNode(category);
        rawNode.setChildren(category.getChildren().stream().map(this::setRawCategoryDetails).collect(Collectors.toList()));
        return rawNode;
    }

    private void setCategoryDetails(Category category) {
        category.setItems(itemRepository.readByCategoryId(category.getId()));
        for (Item item : category.getItems()) {
            item.setAttributes(fieldValueRepository.readByItemId(item.getId()));
        }
        category.setFields(fieldRepository.readByCategoryId(category.getId()));
        category.setChildren(categoryRepository.readByParentId(category.getId()));
        category.getChildren().forEach(this::setCategoryDetails);
    }

    public void saveFullCatalogData(Catalog catalog) {
        catalog.setId(catalogRepository.save(catalog));
        catalog.getRootCategories().forEach(c -> saveCategoryWithChildren(c, catalog.getId()));
    }

    private void saveCategoryWithChildren(Category category, long catalogId) {
        category.setCatalogId(catalogId);
        category.setId(categoryRepository.save(category));
        for (Field field : category.getFields()) {
            field.setCategory(category);
            field.setId(fieldRepository.save(field));
        }
        List<Field> itemFieldCandidates = category.getFields();
        categoryRepository.readAncestors(category).forEach(c ->
                itemFieldCandidates.addAll(fieldRepository.readByCategoryId(c.getId())));
        for (Item item : category.getItems()) {
            item.setCategory(category);
            item.setId(itemRepository.save(item));
            for (Field e : item.getAttributes().keySet()) {
                // todo: add exception
                Field field = itemFieldCandidates.stream().filter(f ->
                        f.getName().equals(e.getName())).findFirst().get();
                e.setId(field.getId());
                e.setCategory(field.getCategory());
                e.setType(field.getType());
            }
            fieldValueRepository.batchSave(item.getId(), item.getAttributes());
        }
        category.getChildren().forEach(c -> c.setParent(category));
        category.getChildren().forEach(c -> saveCategoryWithChildren(c, catalogId));
    }

}
