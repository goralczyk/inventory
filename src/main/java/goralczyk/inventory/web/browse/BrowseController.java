package goralczyk.inventory.web.browse;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Item;
import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.CatalogRepository;
import goralczyk.inventory.repository.CategoryRepository;
import goralczyk.inventory.repository.ItemRepository;
import goralczyk.inventory.web.error.ResourceNotFoundException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Controller
@RequestMapping(value = "/browse")
public class BrowseController {

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @RequestMapping(value = "{catalogTag}/**")
    public String browse(
            HttpServletRequest request,
            @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
            @RequestParam(value = "items", required = false, defaultValue = "10") int itemsPerPage,
            @PathVariable String catalogTag,
            Model model) {

        User user = (User) model.asMap().get("user");

        if (pageNumber < 1 || itemsPerPage < 1) {
            // todo:
            throw new ResourceNotFoundException();
        }

        // get catalog
        Catalog catalog = catalogRepository.readByTagAndUserId(catalogTag, user.getId());
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }

        // get rest of path
        String stringPath = request.getRequestURI();
        if (stringPath.contains("?")) {
            stringPath = stringPath.substring(0, stringPath.indexOf('?'));
        }
        stringPath = stringPath.substring(stringPath.indexOf(catalogTag));
        String currentPath = stringPath;

        // get items and categories
        long numberOfItems;
        Category currentCategory = new Category(0);
        List<Item> items = new ArrayList<>();
        List<Category> childCategories = new ArrayList<>();
        List<Category> categoryPath = new ArrayList<>();

        if (stringPath.equals(catalogTag)) {
            childCategories.addAll(categoryRepository.readRootByCatalogId(catalog.getId()));
            items.addAll(itemRepository.readPaginatedByCatalogId(catalog.getId(), pageNumber, itemsPerPage));
            numberOfItems = itemRepository.countByCatalogId(catalog.getId());
        } else {
            stringPath = stringPath.substring(stringPath.indexOf("/") + 1);
            categoryPath = categoryRepository.readByPath(catalog, stringPath);
            if (categoryPath.isEmpty()) {
                throw new ResourceNotFoundException();
            }
            currentCategory = categoryPath.get(categoryPath.size() - 1);
            childCategories.addAll(categoryRepository.readByParentId(currentCategory.getId()));
            items.addAll(itemRepository.readPaginatedByCategory(currentCategory, pageNumber, itemsPerPage));
            numberOfItems = itemRepository.countByCategoryId(currentCategory.getId());
        }

        // add ancestor categories with paths
        String href = catalogTag;
        List<Map.Entry<String, String>> hrefPath = new ArrayList<>();
        for (Category c : categoryPath) {
            href += "/" + c.getTag();
            hrefPath.add(
                    new AbstractMap.SimpleEntry<>(
                            c.getName(),
                            href
                    )
            );
        }

        // add child categories with paths
        List<Map.Entry<String, String>> hrefChildren = new ArrayList<>();
        for (Category c : childCategories) {
            hrefChildren.add(
                    new AbstractMap.SimpleEntry<>(
                            c.getName(),
                            href + "/" + c.getTag()
                    )
            );
        }

        model.addAttribute("items", items);
        model.addAttribute("hrefPath", hrefPath);
        model.addAttribute("hrefChildren", hrefChildren);
        model.addAttribute("currentCategory", currentCategory);
        model.addAttribute("currentPath", currentPath);
        model.addAttribute("catalog", catalog);
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("itemsPerPage", itemsPerPage);
        model.addAttribute("numberOfItems", numberOfItems);
        model.addAttribute("numberOfPages", (int) (numberOfItems / (float) itemsPerPage) + 1);

        return "browse/browse";
    }
}
