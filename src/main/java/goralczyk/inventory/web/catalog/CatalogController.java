package goralczyk.inventory.web.catalog;

import goralczyk.inventory.model.*;
import goralczyk.inventory.repository.*;
import goralczyk.inventory.web.error.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
@Controller
@RequestMapping(value = "/catalog")
public class CatalogController {

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @RequestMapping(value = "view/{tag}")
    public String viewCatalog(@PathVariable("tag") String tag, Model model) {
        User user = (User) model.asMap().get("user");
        Catalog catalog = user.getCatalogByTag(tag);
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }
        model.addAttribute("currentCatalog", catalog);
        model.addAttribute("numCategories", categoryRepository.countByCatalogId(catalog.getId()));
        model.addAttribute("numItems", itemRepository.countByCatalogId(catalog.getId()));
        return "catalog/view";
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String getNewCatalogForm(Model model) {
        model.addAttribute(new NewCatalogForm());
        return "catalog/new";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String postNewCatalogForm(
            @Valid NewCatalogForm form,
            Errors errors,
            Model model) {
        User user = (User) model.asMap().get("user");

        if (errors.hasErrors()) {
            return "catalog/new";
        }
        if (user.getCatalogByTag(form.getTag()) != null) {
            errors.rejectValue("tag", null, "Duplicate tag");
            return "catalog/new";
        }

        if (form.isCopyExisting()
                && (form.getCatalogToCopyId() == null
                || catalogRepository.read(form.getCatalogToCopyId()) == null)) {
            errors.rejectValue("catalogToCopyId", null, "Please choose existing catalog");
            return "catalog/new";
        }
        if (form.isCopyExisting() && catalogRepository.read(form.getCatalogToCopyId()).getUserId() != user.getId()) {
            throw new AccessDeniedException("Not your catalog.");
        }

        Catalog catalog = new Catalog(form.getName(), form.getTag(), form.getDescription(), user.getId());
        catalog.setId(catalogRepository.save(catalog));

        if (form.isCopyExisting()) {
            categoryRepository.readRootByCatalogId(form.getCatalogToCopyId())
                    .forEach(c -> copyPasteCategory(c, catalog.getId(), form.isCopyItems(), new HashMap<>())
            );
        }

        return "redirect:/catalog/view/" + catalog.getTag();
    }

    private void copyPasteCategory(Category category, long catalogId, boolean withItems, Map<Long, Long> oldNewFieldIdMap) {
        Category newCategory = category.clone();
        newCategory.setCatalogId(catalogId);
        newCategory.setId(categoryRepository.save(newCategory));
        for (Field field : fieldRepository.readByCategoryId(category.getId())) {
            Field newField = field.clone();
            newField.setCategory(newCategory);
            newField.setId(fieldRepository.save(newField));
            oldNewFieldIdMap.put(field.getId(), newField.getId());
        }
        if (withItems) {
            for (Item item : itemRepository.readByCategoryId(category.getId())) {
                Item newItem = item.clone();
                newItem.setCategory(newCategory);
                newItem.setId(itemRepository.save(newItem));
                Map<Field, Object> attributes = fieldValueRepository.readByItemId(item.getId());
                attributes.forEach((f, o) -> f.setId(oldNewFieldIdMap.get(f.getId())));
                fieldValueRepository.batchSave(newItem.getId(), attributes);
            }
        }

        categoryRepository.readByParentId(category.getId())
                .forEach(c -> {
                    c.setParent(newCategory);
                    copyPasteCategory(c, catalogId, withItems, oldNewFieldIdMap);
                });
    }

    @RequestMapping(value = "edit/{tag}", method = RequestMethod.GET)
    public String getEditCatalogForm(@PathVariable("tag") String tag, Model model) {
        User user = (User) model.asMap().get("user");
        Catalog catalog = user.getCatalogByTag(tag);
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }
        EditCatalogForm form = new EditCatalogForm();
        form.setName(catalog.getName());
        form.setTag(catalog.getTag());
        form.setDescription(catalog.getDescription());
        model.addAttribute("editedCatalog", catalog);
        model.addAttribute("editCatalogForm", form);
        return "catalog/edit";
    }

    @RequestMapping(value = "edit/{t}", method = RequestMethod.POST)
    public String postEditCatalogForm(
            @PathVariable("t") String tag,
            @Valid EditCatalogForm form,
            Errors errors,
            Model model) {

        User user = (User) model.asMap().get("user");
        Catalog updatedCatalog = user.getCatalogByTag(tag);

        model.addAttribute("editedCatalog", updatedCatalog);
        if (errors.hasErrors()) {
            return "catalog/edit";
        }
        if (user.getCatalogByTag(form.getTag()) != null
                && !form.getTag().equals(tag)) {
            errors.rejectValue("tag", null, "Duplicate tag");
            return "catalog/edit";
        }
        if (user.getCatalogByTag(tag) == null) {
            throw new ResourceNotFoundException();
        }

        updatedCatalog.setName(form.getName());
        updatedCatalog.setTag(form.getTag());
        updatedCatalog.setDescription(form.getDescription());
        catalogRepository.update(updatedCatalog);
        return "redirect:/catalog/view/" + updatedCatalog.getTag();
    }

    @RequestMapping(value = "delete/{t}", method = RequestMethod.POST)
    public String deleteCatalog(@PathVariable("t") String tag, Model model) {
        User user = (User) model.asMap().get("user");
        Catalog catalog = user.getCatalogByTag(tag);
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }
        catalogRepository.delete(catalog.getId());
        return "redirect:/";
    }
}
