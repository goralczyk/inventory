package goralczyk.inventory.web.catalog;

/**
 * @author Tomasz Góralczyk
 */
class NewCatalogForm extends EditCatalogForm {

    private boolean copyExisting = false;
    private Long catalogToCopyId;
    private boolean copyItems = false;

    public boolean isCopyExisting() {
        return copyExisting;
    }

    public void setCopyExisting(boolean copyExisting) {
        this.copyExisting = copyExisting;
    }

    public Long getCatalogToCopyId() {
        return catalogToCopyId;
    }

    public void setCatalogToCopyId(Long catalogToCopyId) {
        this.catalogToCopyId = catalogToCopyId;
    }

    public boolean isCopyItems() {
        return copyItems;
    }

    public void setCopyItems(boolean copyItems) {
        this.copyItems = copyItems;
    }
}
