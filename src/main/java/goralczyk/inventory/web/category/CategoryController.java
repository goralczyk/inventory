package goralczyk.inventory.web.category;

import com.fasterxml.jackson.core.JsonProcessingException;
import goralczyk.inventory.model.*;
import goralczyk.inventory.repository.*;
import goralczyk.inventory.service.CustomUserDetails;
import goralczyk.inventory.service.JsonService;
import goralczyk.inventory.web.error.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Controller
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private JsonService jsonService;

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String getNewCategory(
            @RequestParam("catalog") String catalogTag,
            Model model
    ) throws JsonProcessingException {
        User user = (User) model.asMap().get("user");
        Catalog catalog = user.getCatalogByTag(catalogTag);
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }
        model.addAttribute(new NewCategoryForm());
        model.addAttribute("catalogJsonData", jsonService.exportCatalogsRaw(Arrays.asList(catalog)));
        return "category/new";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String postNewCategory(
            @RequestParam("catalog") String catalogTag,
            @Valid NewCategoryForm form,
            Errors errors,
            Model model
    ) throws JsonProcessingException {
        User user = (User) model.asMap().get("user");
        Catalog catalog = user.getCatalogByTag(catalogTag);
        validateNew(user, catalog, form, errors);
        if (errors.hasErrors()) {
            model.addAttribute(form);
            model.addAttribute("catalogJsonData", jsonService.exportCatalogsRaw(Arrays.asList(catalog)));
            return "category/new";
        }
        Category category = form.toCategory(catalog.getId());
        category.setId(categoryRepository.save(category));
        form.getFields().stream().map(fp -> fp.toField(category)).forEach(fieldRepository::save);
        return "redirect:/category/view?id=" + category.getId();
    }

    private void validateNew(User user, Catalog catalog, NewCategoryForm form, Errors errors) {
        if (catalog == null) {
            throw new ResourceNotFoundException();
        }
        Long parentCategoryId = form.getParentId();
        List<Category> hierarchyCategories = new ArrayList<>();
        List<Category> siblingCategories = new ArrayList<>();
        if (parentCategoryId == null) {
            siblingCategories.addAll(categoryRepository.readRootByCatalogId(catalog.getId()));
        } else {
            Category parent = categoryRepository.read(parentCategoryId);
            if (parent == null) {
                throw new ResourceNotFoundException();
            }
            Catalog parentCatalog = catalogRepository.read(parent.getCatalogId());
            if (parentCatalog.getUserId() != user.getId()) {
                throw new ResourceNotFoundException();
            }
            hierarchyCategories.addAll(categoryRepository.readAncestors(parent));
            hierarchyCategories.add(parent);
            siblingCategories.addAll(categoryRepository.readByParentId(parentCategoryId));
        }
        validateTagAndName(form, siblingCategories, errors);
        validateFields(form.getFields(), hierarchyCategories, errors);
    }

    private void validateEdited(Category category, EditCategoryForm form, Errors errors) {
        List<Category> hierarchyCategories = new ArrayList<>();
        List<Category> siblingCategories = new ArrayList<>();
        hierarchyCategories.addAll(categoryRepository.readAncestors(category));
        hierarchyCategories.add(category);
        hierarchyCategories.addAll(categoryRepository.readDescendants(category.getId()));
        if (category.getParentId() == null) {
            siblingCategories.addAll(categoryRepository.readRootByCatalogId(category.getCatalogId()));
        } else {
            siblingCategories.addAll(categoryRepository.readByParentId(category.getParentId()));
        }
        siblingCategories.remove(category);
        validateTagAndName(form, siblingCategories, errors);
        validateFields(form.getFields(), hierarchyCategories, errors);
    }

    private void validateTagAndName(EditCategoryForm form, List<Category> siblingCategories, Errors errors) {
        for (Category s : siblingCategories) {
            if (s.getTag().equals(form.getTag())) {
                errors.rejectValue("tag", null, "Duplicate tag");
            }
            if (s.getName().equals(form.getName())) {
                errors.rejectValue("name", null, "Duplicate name");
            }
        }
    }

    private void validateFields(List<FieldProxy> formFields, List<Category> hierarchyCategories, Errors errors) {
        for (Category c : hierarchyCategories) {
            for (Field f : fieldRepository.readByCategoryId(c.getId())) {
                for (int i = 0; i < formFields.size(); ++i) {
                    if (f.getName().equals(formFields.get(i).getName())) {
                        if (f.getId() != formFields.get(i).getFieldId()) {
                            errors.rejectValue("fields[" + i + "]", null, "Duplicate field in hierarchy");
                        }
                    }
                }
            }
        }
        for (int i = 0; i < formFields.size(); ++i) {
            for (int j = i + 1; j < formFields.size(); ++j) {
                if (formFields.get(i).getName().equals(formFields.get(j).getName())) {
                    errors.rejectValue("fields[" + i + "]", null, "Duplicate fields");
                    errors.rejectValue("fields[" + j + "]", null, "Duplicate fields");
                }
            }
        }
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String getEditCategory(
            @RequestParam("id") long categoryId,
            Model model
    ) {
        User user = (User) model.asMap().get("user");
        Category category = validateAccessAndGetCategory(user.getId(), categoryId);
        category.setFields(fieldRepository.readByCategoryId(category.getId()));
        model.addAttribute(new EditCategoryForm(category));
        model.addAttribute("saveItemsDisabled", category.getParent() == null);
        return "category/edit";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String postEditCategory(
            @RequestParam("id") long categoryId,
            @Valid EditCategoryForm form,
            Errors errors,
            Model model
    ) {
        User user = (User) model.asMap().get("user");
        Category category = validateAccessAndGetCategory(user.getId(), categoryId);
        validateEdited(category, form, errors);
        if (errors.hasErrors()) {
            model.addAttribute(form);
            model.addAttribute("saveItemsDisabled", category.getParent() == null);
            return "category/edit";
        }
        List<Field> originalFields = fieldRepository.readByCategoryId(categoryId);
        category.setName(form.getName());
        category.setTag(form.getTag());
        category.setDescription(form.getDescription());
        categoryRepository.update(category);
        List<Field> formFields = form.getFields().stream().map(fp -> fp.toField(category)).collect(Collectors.toList());
        formFields.forEach(f -> {
            if (f.getId() == 0) {
                fieldRepository.save(f);
            } else {
                fieldRepository.update(f);
            }
        });
        originalFields.stream().filter(of -> !formFields.contains(of)).forEach(f -> fieldRepository.delete(f.getId()));
        return "redirect:/category/view?id=" + category.getId();
    }

    @RequestMapping(value = "delete")
    public String deleteCategory(
            @RequestParam("id") long categoryId,
            @RequestParam("delete_items") boolean deleteItems,
            Model model
    ) {
        User user = (User) model.asMap().get("user");
        Category category = validateAccessAndGetCategory(user.getId(), categoryId);
        if (deleteItems) {
            categoryRepository.delete(categoryId);
        } else {
            Long parentId = category.getParentId();
            if (parentId == null) {
                throw new ResourceNotFoundException();
            }

            Category parent = categoryRepository.read(parentId);
            List<Item> items = itemRepository.readPaginatedByCategory(category, 1, Integer.MAX_VALUE);

            // set attributes
            List<Field> fieldsToDelete = new ArrayList<>();
            fieldsToDelete.addAll(fieldRepository.readByCategoryId(categoryId));
            categoryRepository.readDescendants(parentId).forEach(c -> fieldsToDelete.addAll(c.getFields()));
            items.forEach(i -> {
                i.setAttributes(fieldValueRepository.readByItemId(i.getId()).entrySet().stream().filter(e ->
                        !fieldsToDelete.contains(e.getKey()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                i.setCategory(parent);
            });

            // cascade delete
            categoryRepository.delete(categoryId);

            // save
            items.forEach(i -> {
                i.setId(itemRepository.save(i));
                fieldValueRepository.batchSave(i.getId(), i.getAttributes());
            });
        }
        return "redirect:/";
    }

    @RequestMapping(value = "view")
    public String viewCategory(@RequestParam("id") long categoryId, Model model) {
        User user = (User) model.asMap().get("user");
        Category category = validateAccessAndGetCategory(user.getId(), categoryId);
        List<Category> categories = categoryRepository.readAncestors(category);
        categories.add(category);
        String path = categories.stream().map(Category::getTag).collect(Collectors.joining("/"));
        Catalog catalog = catalogRepository.read(category.getCatalogId());
        return "redirect:/browse/" + catalog.getTag() + "/" + path;
    }

    private Category validateAccessAndGetCategory(long userId, long categoryId) {
        Category category = categoryRepository.read(categoryId);
        if (category == null) {
            throw new ResourceNotFoundException();
        }
        Catalog catalog = catalogRepository.read(category.getCatalogId());
        if (catalog.getUserId() != userId) {
            throw new ResourceNotFoundException();
        }
        return category;
    }
}
