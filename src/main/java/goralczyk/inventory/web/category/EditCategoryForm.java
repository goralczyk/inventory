package goralczyk.inventory.web.category;

import goralczyk.inventory.model.Category;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
class EditCategoryForm {

    @NotBlank
    @Size(max = 128)
    private String name;

    @NotBlank
    @Size(max = 128)
    @Pattern(regexp = "^[a-z0-9_-]*$", message = "Only numbers, lower-case letters and digits.")
    private String tag;

    @Size(max = 256)
    private String description;

    private List<FieldProxy> fields;

    public EditCategoryForm() {
        fields = new ArrayList<>();
    }

    public EditCategoryForm(Category category) {
        this.name = category.getName();
        this.tag = category.getTag();
        this.description = category.getDescription();
        this.fields = category.getFields().stream().map(FieldProxy::new).collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FieldProxy> getFields() {
        return fields;
    }

    public void setFields(List<FieldProxy> fields) {
        this.fields = fields;
    }
}
