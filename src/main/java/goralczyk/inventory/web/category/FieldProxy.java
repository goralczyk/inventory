package goralczyk.inventory.web.category;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;

/**
 * @author Tomasz Góralczyk
 */
public class FieldProxy {

    private int typeCode;
    private String name = "";
    private long fieldId;

    public FieldProxy() {
    }

    public FieldProxy(Field field) {
        this.typeCode = field.getType().getTypeCode();
        this.name = field.getName();
        this.fieldId = field.getId();
    }

    public FieldProxy(int typeCode, String name, long fieldId) {
        this.typeCode = typeCode;
        this.name = name;
        this.fieldId = fieldId;
    }

    public int getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    public Field toField(Category category) {
        return new Field(fieldId, name, FieldType.getByTypeCode(typeCode), category);
    }
}
