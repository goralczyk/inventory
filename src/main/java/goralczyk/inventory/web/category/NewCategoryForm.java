package goralczyk.inventory.web.category;

import goralczyk.inventory.model.Category;

/**
 * @author Tomasz Góralczyk
 */
class NewCategoryForm extends EditCategoryForm {

    private Long parentId;

    public NewCategoryForm() {
        super();
    }

    public NewCategoryForm(Category category) {
        super(category);
        parentId = category.getParentId();
    }

    public Category toCategory(long catalogId) {
        return new Category(super.getName(), super.getTag(), super.getDescription(), catalogId, parentId == null ? null : new Category(parentId));
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
