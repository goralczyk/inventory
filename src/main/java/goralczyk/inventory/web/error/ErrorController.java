package goralczyk.inventory.web.error;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class ErrorController {

    @RequestMapping("/error")
    public String error(HttpServletRequest request, Model model) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            model.addAttribute("message", "Unknown exception");
            return "error/general";
        }
        String statusMessage = HttpStatus.valueOf(statusCode).getReasonPhrase();
        model.addAttribute("message", statusCode + " - " + statusMessage);
        return "error/general";
    }
}
