package goralczyk.inventory.web.export;

import com.fasterxml.jackson.core.JsonProcessingException;
import goralczyk.inventory.repository.CatalogRepository;
import goralczyk.inventory.service.CustomUserDetails;
import goralczyk.inventory.service.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class ExportController {

    @Autowired
    private JsonService jsonService;

    @Autowired
    private CatalogRepository catalogRepository;

    @RequestMapping(value = "/export", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String exportAll(@AuthenticationPrincipal CustomUserDetails customUserDetails) throws JsonProcessingException {
        return jsonService.exportCatalogs(
                catalogRepository.readByUserId(customUserDetails.getId()));
    }
}
