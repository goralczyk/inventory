package goralczyk.inventory.web.global;

import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.CatalogRepository;
import goralczyk.inventory.repository.UserRepository;
import goralczyk.inventory.service.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.LinkedHashSet;

/**
 * Populates model with commonly used data.
 *
 * @author Tomasz Góralczyk
 */
@ControllerAdvice
public class GlobalControllerAdvice {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CatalogRepository catalogRepository;

    @ModelAttribute
    public void getNavInfo(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        if (customUserDetails == null) {
            return;
        }
        int id = customUserDetails.getId();
        User user = userRepository.read(id);
        user.setCatalogs(catalogRepository.readByUserId(id));
        model.addAttribute(user);
    }
}
