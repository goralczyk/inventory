package goralczyk.inventory.web.home;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.CatalogRepository;
import goralczyk.inventory.repository.CategoryRepository;
import goralczyk.inventory.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class HomeController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @RequestMapping(value = {"/", "/home"})
    public String home(Model model) {
        Map<String, Object> modelMap = model.asMap();
        User user = (User) modelMap.get("user");
        int numCategories = 0;
        int numItems = 0;
        for (Catalog c : user.getCatalogs()) {
            numCategories += categoryRepository.countByCatalogId(c.getId());
            numItems += itemRepository.countByCatalogId(c.getId());
        }
        model.addAttribute("numCategories", numCategories);
        model.addAttribute("numItems", numItems);
        return "home/userhome";
    }
}