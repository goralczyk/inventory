package goralczyk.inventory.web.info;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class InfoController {

    @RequestMapping("/about")
    public String about() {
        return "info/about";
    }
}
