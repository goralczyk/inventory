package goralczyk.inventory.web.item;

import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.Item;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
class EditItemForm {

    @NotBlank
    @Size(max = 128)
    private String name;

    @Size(max = 256)
    private String description;

    @Min(0)
    private long quantity;

    private Map<Field, Object> itemAttributes;
    private Map<Long, String> inputAttributes;

    public EditItemForm() {
    }

    public EditItemForm(Item item) {
        this.name = item.getName();
        this.description = item.getDescription();
        this.quantity = item.getQuantity();
        this.itemAttributes = item.getAttributes();
        this.inputAttributes = new HashMap<>();
        item.getAttributes().forEach((f, o) -> inputAttributes.put(f.getId(), String.valueOf(o)));
    }

    public void inputToItemAttributes() {
        for (Map.Entry<Field, Object> itemAttribute : itemAttributes.entrySet()) {
            itemAttribute.setValue(inputAttributes.get(itemAttribute.getKey().getId()));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Map<Field, Object> getItemAttributes() {
        return itemAttributes;
    }

    public void setItemAttributes(Map<Field, Object> itemAttributes) {
        this.itemAttributes = itemAttributes;
    }

    public Map<Long, String> getInputAttributes() {
        return inputAttributes;
    }

    public void setInputAttributes(Map<Long, String> inputAttributes) {
        this.inputAttributes = inputAttributes;
    }
}
