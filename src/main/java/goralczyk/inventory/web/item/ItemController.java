package goralczyk.inventory.web.item;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import goralczyk.inventory.model.*;
import goralczyk.inventory.repository.*;
import goralczyk.inventory.service.JsonService;
import goralczyk.inventory.web.error.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Tomasz Góralczyk
 */
@Controller
@RequestMapping(value = "/item")
public class ItemController {

    @Autowired
    private JsonService jsonService;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String getEditItemForm(@RequestParam(value = "id") long id, Model model) {
        Item item = validateAccessAndGetItem(id, (User) model.asMap().get("user"));
        getItemAttributes(item);
        model.addAttribute(new EditItemForm(item));
        return "item/edit";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String postEditItemForm(
            @RequestParam(value = "id") long id,
            @Valid EditItemForm form,
            Errors errors,
            Model model) {
        Item item = validateAccessAndGetItem(id, (User) model.asMap().get("user"));
        if (errors.hasErrors()) {
            form.setItemAttributes(fieldValueRepository.readByItemId(item.getId()));
            return "item/edit";
        }
        item.setName(form.getName());
        item.setDescription(form.getDescription());
        item.setQuantity(form.getQuantity());
        itemRepository.update(item);

        form.setItemAttributes(getItemAttributes(item));
        form.inputToItemAttributes();
        // todo: validate inputs
        fieldValueRepository.batchSave(id, form.getItemAttributes());
        return "redirect:/item/view?id=" + id;
    }

    private Map<Field, Object> getItemAttributes(Item item) {
        List<Category> categories = new ArrayList<>();
        categories.addAll(categoryRepository.readAncestors(item.getCategory()));
        categories.add(item.getCategory());
        Map<Field, Object> attributes = new LinkedHashMap<>();
        for (Category category : categories) {
            fieldRepository.readByCategoryId(category.getId()).forEach(f -> attributes.put(f, ""));
        }
        attributes.putAll(fieldValueRepository.readByItemId(item.getId()));
        item.setAttributes(attributes);
        return item.getAttributes();
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteItem(@RequestParam(value = "id") long id, Model model) {
        validateAccessAndGetItem(id, (User) model.asMap().get("user"));
        itemRepository.delete(id);
        return "redirect:/";
    }

    @RequestMapping(value = "view")
    public String viewItem(@RequestParam(value = "id") long id, Model model) {
        Item item = validateAccessAndGetItem(id, (User) model.asMap().get("user"));
        getItemAttributes(item);
        Category category = item.getCategory();
        Catalog catalog = catalogRepository.read(category.getCatalogId());
        // find path
        List<Category> categoryList = categoryRepository.readAncestors(category);
        categoryList.add(category);
        Map.Entry<String, String> nameTagPathPair =
                new AbstractMap.SimpleEntry<>(
                        catalog.getName() + "/" + categoryList.stream().map(AbstractEntity::getName).collect(Collectors.joining("/")),
                        catalog.getTag() + "/" + categoryList.stream().map(Category::getTag).collect(Collectors.joining("/"))
                );

        model.addAttribute(item);
        model.addAttribute("path", nameTagPathPair);
        return "item/view";
    }

    private Item validateAccessAndGetItem(long itemId, User user) {
        Item item = itemRepository.read(itemId);
        if (item == null) {
            throw new ResourceNotFoundException();
        }
        Category category = categoryRepository.read(item.getCategory().getId());
        Catalog catalog = catalogRepository.read(category.getCatalogId());
        // validate item access
        if (catalog.getUserId() != user.getId()) {
            throw new ResourceNotFoundException();
        }
        item.setCategory(category);
        return item;
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String getNewItemForm(
            @RequestParam(value = "catalog", required = false) String catalogTag,
            Model model) throws JsonProcessingException {
        User user = (User) model.asMap().get("user");
        prepareModel(user, catalogTag, model);
        model.addAttribute(new NewItemForm());
        return "item/new";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String postNewItemForm(
            @RequestParam(value = "catalog", required = false) String catalogTag,
            @Valid NewItemForm form,
            Errors errors,
            Model model) throws JsonProcessingException {
        User user = (User) model.asMap().get("user");
        Category category = categoryRepository.read(form.getCategoryId());
        if (category == null) {
            throw new ResourceNotFoundException();
        }
        Catalog catalog = catalogRepository.read(category.getCatalogId());
        if (user.getCatalogByTag(catalog.getTag()) == null) {
            throw new ResourceNotFoundException();
        }
        if (errors.hasErrors()) {
            prepareModel(user, catalogTag, model);
            return "item/new";
        }
        long itemId = itemRepository.save(form.getItem());
        return "redirect:/item/view?id=" + itemId;
    }

    private void prepareModel(User user, String catalogTag, Model model) throws JsonProcessingException {
        if (catalogTag == null || catalogTag.equals("")) {
            model.addAttribute("catalogsJsonData", jsonService.exportCatalogsRaw(user.getCatalogs()));
        } else {
            Catalog catalog = user.getCatalogByTag(catalogTag);
            model.addAttribute("catalogsJsonData", jsonService.exportCatalogsRaw(Arrays.asList(catalog)));
            if (catalog == null) {
                throw new ResourceNotFoundException();
            }
        }
    }
}
