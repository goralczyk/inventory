package goralczyk.inventory.web.item;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Item;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Tomasz Góralczyk
 */
class NewItemForm {

    @NotNull
    private Long categoryId;

    @NotBlank
    @Size(max = 128)
    private String name;

    @Size(max = 256)
    private String description;

    @Min(0)
    private long quantity;

    public Item getItem() {
        return new Item(name, description, quantity, new Category(categoryId));
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
