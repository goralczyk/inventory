package goralczyk.inventory.web.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String logIn() {
        return "login/login";
    }
}
