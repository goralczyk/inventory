package goralczyk.inventory.web.password;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Tomasz Góralczyk
 */
// todo: pass messaging functionality to another view
@Controller
@RequestMapping(value = "restorepassword")
public class RestorePasswordController {

    @RequestMapping(method = RequestMethod.GET)
    public String restorePassword() {
        return "password/restorepassword";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String restorePasswordInstructions() {
        return "password/restorepasswordinstructions";
    }

    @RequestMapping(method = RequestMethod.GET, params = "token")
    public String askNewPassword(@RequestParam(value = "token") String token) {
        return "password/restorepasswordchange";
    }

    @RequestMapping(method = RequestMethod.POST, params = "token")
    public String setNewPassword(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "password") String password) {
        return "password/restorepasswordchange";
    }
}
