package goralczyk.inventory.web.register;

import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class RegisterController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterForm(Model model) {
        model.addAttribute(new RegisterForm());
        return "register/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String postRegisterForm(@Valid RegisterForm registerForm, Errors errors) {
        if (errors.hasErrors()) {
            return "register/register";
        }
        User user = registerForm.createUser();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        try {
            userRepository.save(user);
        } catch (DuplicateKeyException e) {
            errors.rejectValue("email", null, "Already registered");
            return "register/register";
        }
        return "redirect:/login?registered";
    }
}
