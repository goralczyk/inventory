package goralczyk.inventory.web.search;

import com.fasterxml.jackson.core.JsonProcessingException;
import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.CatalogRepository;
import goralczyk.inventory.service.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Tomasz Góralczyk
 */
@Controller
public class QuickSearchController {

    @Autowired
    private JsonService jsonService;

    @Autowired
    private CatalogRepository catalogRepository;

    @RequestMapping(value = "/search")
    public String quickSearch(
            @RequestParam(value = "catalog") String catalogTag,
            Model model) throws JsonProcessingException {
        User user = (User) model.asMap().get("user");
        Catalog catalog = catalogRepository.readByTagAndUserId(catalogTag, user.getId());
        model.addAttribute("treeViewData", jsonService.exportCatalogTreeView(catalog));
        model.addAttribute(catalog);
        return "search/quicksearch";
    }
}
