package goralczyk.inventory.web.settings;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import static goralczyk.inventory.web.register.RegisterForm.MAX_NAME_LENGTH;
import static goralczyk.inventory.web.register.RegisterForm.MIN_NAME_LENGTH;

/**
 * @author Tomasz Góralczyk
 */
class NameForm {

    @NotBlank
    @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String name;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
