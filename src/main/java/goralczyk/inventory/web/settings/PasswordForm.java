package goralczyk.inventory.web.settings;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import static goralczyk.inventory.web.register.RegisterForm.MAX_PASSWORD_LENGTH;
import static goralczyk.inventory.web.register.RegisterForm.MIN_PASSWORD_LENGTH;

/**
 * @author Tomasz Góralczyk
 */
class PasswordForm {

    private String oldPassword;

    @NotBlank
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    private String newPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
