package goralczyk.inventory.web.settings;

import goralczyk.inventory.model.User;
import goralczyk.inventory.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

import static goralczyk.inventory.web.register.RegisterForm.*;

/**
 * @author Tomasz Góralczyk
 */
@Controller
@RequestMapping(value = "/settings")
public class SettingsController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "name", method = RequestMethod.GET)
    public String getChangeName(Model model) {
        model.addAttribute(new NameForm());
        addNameConstants(model);
        return "settings/name";
    }

    @RequestMapping(value = "name", method = RequestMethod.POST)
    public String postChangeName(@Valid NameForm nameForm, Errors errors, Model model) {
        User user = (User) model.asMap().get("user");
        if (!passwordEncoder.matches(nameForm.getPassword(), user.getPassword())) {
            errors.rejectValue("password", null, "Incorrect password");
            addNameConstants(model);
            return "settings/name";
        }
        if (errors.hasErrors()) {
            addNameConstants(model);
            return "settings/name";
        }
        user.setName(nameForm.getName());
        userRepository.update(user);
        return "redirect:/";
    }

    @RequestMapping(value = "password", method = RequestMethod.GET)
    public String getChangePassword(Model model) {
        model.addAttribute(new PasswordForm());
        addPasswordConstants(model);
        return "settings/password";
    }

    @RequestMapping(value = "password", method = RequestMethod.POST)
    public String postChangePassword(@Valid PasswordForm passwordForm, Errors errors, Model model) {
        User user = (User) model.asMap().get("user");
        if (!passwordEncoder.matches(passwordForm.getOldPassword(), user.getPassword())) {
            errors.rejectValue("oldPassword", null, "Incorrect password");
            addPasswordConstants(model);
            return "settings/password";
        }
        if (errors.hasErrors()) {
            addPasswordConstants(model);
            return "settings/password";
        }
        user.setPassword(passwordEncoder.encode(passwordForm.getNewPassword()));
        userRepository.update(user);
        return "redirect:/";
    }

    private void addNameConstants(Model model) {
        model.addAttribute("MIN_NAME_LENGTH", MIN_NAME_LENGTH);
        model.addAttribute("MAX_NAME_LENGTH", MAX_NAME_LENGTH);
    }

    private void addPasswordConstants(Model model) {
        model.addAttribute("MIN_PASSWORD_LENGTH", MIN_PASSWORD_LENGTH);
        model.addAttribute("MAX_PASSWORD_LENGTH", MAX_PASSWORD_LENGTH);
    }
}
