-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2016-07-15 19:06:02.486

-- tables
-- Table: Catalog
CREATE TABLE Catalog (
  id          INT          NOT NULL AUTO_INCREMENT,
  name        VARCHAR(128) NOT NULL,
  tag         VARCHAR(128) NOT NULL,
  description VARCHAR(256) NOT NULL,
  user_id     INT          NOT NULL,
  UNIQUE INDEX Catalog_ak_1 (user_id, tag),
  CONSTRAINT Catalog_pk PRIMARY KEY (id)
);

-- Table: Category
CREATE TABLE Category (
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(128) NOT NULL,
  tag         VARCHAR(128) NOT NULL,
  description VARCHAR(256) NOT NULL,
  catalog_id  INT          NOT NULL,
  parent_id   BIGINT       NULL,
  UNIQUE INDEX Category_ak_1 (catalog_id, parent_id, tag),
  CONSTRAINT Category_pk PRIMARY KEY (id)
);

-- Table: Field
CREATE TABLE Field (
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  type        TINYINT  NOT NULL,
  name        VARCHAR(128) NOT NULL,
  category_id BIGINT       NOT NULL,
  UNIQUE INDEX Field_ak_1 (category_id, name),
  CONSTRAINT Field_pk PRIMARY KEY (id)
);

-- Table: FieldValueDate
CREATE TABLE FieldValueDate (
  value    DATE   NOT NULL,
  item_id  BIGINT NOT NULL,
  field_id BIGINT NOT NULL,
  CONSTRAINT FieldValueDate_pk PRIMARY KEY (item_id, field_id)
);

-- Table: FieldValueNumber
CREATE TABLE FieldValueNumber (
  value    DECIMAL(9, 3) NULL,
  item_id  BIGINT        NOT NULL,
  field_id BIGINT        NOT NULL,
  CONSTRAINT FieldValueNumber_pk PRIMARY KEY (item_id, field_id)
);

-- Table: FieldValueString
CREATE TABLE FieldValueString (
  value    VARCHAR(256) NOT NULL,
  item_id  BIGINT       NOT NULL,
  field_id BIGINT       NOT NULL,
  CONSTRAINT FieldValueString_pk PRIMARY KEY (item_id, field_id)
);

-- Table: Item
CREATE TABLE Item (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  name         VARCHAR(128) NOT NULL,
  description  VARCHAR(256) NOT NULL,
  quantity     INT UNSIGNED NOT NULL DEFAULT 1,
  time_created TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  category_id  BIGINT       NOT NULL,
  CONSTRAINT Item_pk PRIMARY KEY (id)
);

-- Table: User
CREATE TABLE `User` (
  id        INT          NOT NULL AUTO_INCREMENT,
  name      VARCHAR(64)  NOT NULL,
  email     VARCHAR(256) NOT NULL,
  password  CHAR(80)     NOT NULL,
  activated BOOL         NOT NULL DEFAULT TRUE,
  UNIQUE INDEX User_ak_1 (email),
  CONSTRAINT User_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: Category_Catalog (table: Category)
ALTER TABLE Category
  ADD CONSTRAINT Category_Catalog FOREIGN KEY (catalog_id)
REFERENCES Catalog (id)
  ON DELETE CASCADE;

-- Reference: Category_Category (table: Category)
ALTER TABLE Category
  ADD CONSTRAINT Category_Category FOREIGN KEY (parent_id)
REFERENCES Category (id)
  ON DELETE CASCADE;

-- Reference: Category_Field (table: Field)
ALTER TABLE Field
  ADD CONSTRAINT Category_Field FOREIGN KEY (category_id)
REFERENCES Category (id)
  ON DELETE CASCADE;

-- Reference: FieldValueDate_Field (table: FieldValueDate)
ALTER TABLE FieldValueDate
  ADD CONSTRAINT FieldValueDate_Field FOREIGN KEY (field_id)
REFERENCES Field (id)
  ON DELETE CASCADE;

-- Reference: FieldValueDate_Item (table: FieldValueDate)
ALTER TABLE FieldValueDate
  ADD CONSTRAINT FieldValueDate_Item FOREIGN KEY (item_id)
REFERENCES Item (id)
  ON DELETE CASCADE;

-- Reference: FieldValueNumber_Field (table: FieldValueNumber)
ALTER TABLE FieldValueNumber
  ADD CONSTRAINT FieldValueNumber_Field FOREIGN KEY (field_id)
REFERENCES Field (id)
  ON DELETE CASCADE;

-- Reference: FieldValueNumber_Item (table: FieldValueNumber)
ALTER TABLE FieldValueNumber
  ADD CONSTRAINT FieldValueNumber_Item FOREIGN KEY (item_id)
REFERENCES Item (id)
  ON DELETE CASCADE;

-- Reference: FieldValueString_Field (table: FieldValueString)
ALTER TABLE FieldValueString
  ADD CONSTRAINT FieldValueString_Field FOREIGN KEY (field_id)
REFERENCES Field (id)
  ON DELETE CASCADE;

-- Reference: FieldValueString_Item (table: FieldValueString)
ALTER TABLE FieldValueString
  ADD CONSTRAINT FieldValueString_Item FOREIGN KEY (item_id)
REFERENCES Item (id)
  ON DELETE CASCADE;

-- Reference: Item_Category (table: Item)
ALTER TABLE Item
  ADD CONSTRAINT Item_Category FOREIGN KEY (category_id)
REFERENCES Category (id)
  ON DELETE CASCADE;

-- Reference: User_Catalog (table: Catalog)
ALTER TABLE Catalog
  ADD CONSTRAINT User_Catalog FOREIGN KEY (user_id)
REFERENCES `User` (id)
  ON DELETE CASCADE;

-- End of file.
