package goralczyk.inventory.repository;

import goralczyk.inventory.model.Catalog;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class CatalogRepositoryTest {

    @Autowired
    private CatalogRepository catalogRepository;

    @Test
    public void shouldSave() {
        long id = catalogRepository.save(new Catalog("name", "tag", "desc", 1));
        Assert.assertEquals(4, id);
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSaveDuplicateTag() {
        catalogRepository.save(new Catalog("name1", "tag1", "desc1", 1));
    }

    @Test
    public void shouldReadByUserIdSortedByName() {
        // when
        List<Catalog> found = catalogRepository.readByUserId(1);

        // then
        Assert.assertNotNull(found);
        Assert.assertEquals(3, found.size());
        // assert order
        List<Catalog> foundSorted = new ArrayList<>(found);
        foundSorted.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
        Assert.assertEquals(foundSorted, found);
    }

    @Test
    public void shouldReadByTagAndUserId() {
        // given
        Catalog catalog = new Catalog(1, "name1", "tag1", "desc1", 1);

        // when
        Catalog readCatalog = catalogRepository.readByTagAndUserId("tag1", 1);

        // then
        Assert.assertTrue(EqualsBuilder.reflectionEquals(catalog, readCatalog));
    }

    @Test
    public void shouldUpdate() {
        // given
        Catalog catalog = new Catalog(1, "name", "tag", "desc", 1);

        // when
        catalogRepository.update(catalog);
        Catalog readCatalog = catalogRepository.read(1);

        // then
        Assert.assertTrue(EqualsBuilder.reflectionEquals(catalog, readCatalog));
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotUpdateDuplicateTag() {
        // given
        Catalog catalog = new Catalog(2, "", "tag1", "", 1);

        // when
        catalogRepository.update(catalog);
    }

    @Test
    public void shouldDelete() {
        catalogRepository.delete(1);
        Assert.assertNull(catalogRepository.read(1));
    }
}
