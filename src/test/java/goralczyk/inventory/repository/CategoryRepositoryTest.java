package goralczyk.inventory.repository;

import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void shouldSave() {
        long id = categoryRepository.save(new Category("name", "tag", "desc", 1, null));
        Assert.assertEquals(6, id);
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSave() {
        categoryRepository.save(new Category("name1", "tag1", "desc1", 1, null));
    }

    @Test
    public void shouldRead() {
        // given
        Category category = new Category(1, "name1", "tag1", "desc1", 1, null);

        // when
        Category readCategory = categoryRepository.read(1);

        // then
        Assert.assertTrue(EqualsBuilder.reflectionEquals(category, readCategory));
    }

    @Test
    public void shouldReadByPath() {
        // given
        Catalog catalog = new Catalog(1, "", "", "", 1);
        String path = "tag1/tag2/tag3/";

        for (String p : path.split("/")) {
            System.out.println(p);
        }

        // when
        List<Category> readCategories = categoryRepository.readByPath(catalog, path);

        // then
        Assert.assertEquals(3, readCategories.size());
    }

    @Test
    public void shouldNotReadByPath() {
        // given
        Catalog catalog = new Catalog(1, "", "", "", 1);
        String path = "tag2/tag3";

        // when
        List<Category> readCategories = categoryRepository.readByPath(catalog, path);

        // then
        Assert.assertTrue(readCategories.isEmpty());
    }

    @Test
    public void shouldReadByParentId() {
        // when
        List<Category> readCategories = categoryRepository.readByParentId(1);

        // then
        Assert.assertEquals(2, readCategories.size());
        for (Category category : readCategories) {
            Assert.assertEquals(new Long(1), category.getParentId());
        }
    }

    @Test
    public void shouldReadAncestors() {
        // given
        List<Category> categories = Arrays.asList(
                new Category(1, "name1", "tag1", "desc1", 1, null),
                new Category(2, "name2", "tag2", "desc2", 1, new Category(1))
        );

        // when
        List<Category> readCategories = categoryRepository.readAncestors(categoryRepository.read(3));

        // then
        ReflectionAssert.assertReflectionEquals(categories, readCategories);
    }

    @Test
    public void shouldReadDescendants() {
        Assert.assertEquals(3, categoryRepository.readDescendants(1).size());
    }

    @Test
    public void shouldReadRootByCatalogId() {
        // when
        List<Category> readCategories = categoryRepository.readRootByCatalogId(1);

        // then
        Assert.assertEquals(2, readCategories.size());
    }

    @Test
    public void shouldReadByCatalogId() {
        // when
        List<Category> readCategories = categoryRepository.readByCatalogId(1);

        // then
        Assert.assertEquals(5, readCategories.size());
    }

    @Test
    public void shouldCountByCatalogId() {
        Assert.assertEquals(5, categoryRepository.countByCatalogId(1));
    }

    @Test
    public void shouldUpdate() {
        // given
        Category category = new Category(1, "name", "tag", "desc", 1, null);

        // when
        categoryRepository.update(category);
        Category readCategory = categoryRepository.read(1);

        // then
        Assert.assertTrue(EqualsBuilder.reflectionEquals(category, readCategory, "fields"));
    }

    @Test
    public void shouldDelete() {
        categoryRepository.delete(1);
        Assert.assertNull(categoryRepository.read(1));
    }
}
