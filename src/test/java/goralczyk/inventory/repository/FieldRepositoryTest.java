package goralczyk.inventory.repository;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class FieldRepositoryTest {

    @Autowired
    private FieldRepository fieldRepository;

    @Test
    public void shouldSave() {
        // given
        Field field = new Field(4, "name5", FieldType.NUMBER, new Category(1));

        // when
        long id = fieldRepository.save(field);
        Field readField = fieldRepository.read(4);

        // then
        Assert.assertEquals(4, id);
        Assert.assertEquals(field, readField);
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSaveParentLevelDKE() {
        fieldRepository.save(new Field("name2", FieldType.NUMBER, new Category(1)));
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSaveSameLevelDKE() {
        fieldRepository.save(new Field("name2", FieldType.NUMBER, new Category(2)));
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSaveChildLevelDKE() {
        fieldRepository.save(new Field("name2", FieldType.NUMBER, new Category(3)));
    }

    @Test
    public void shouldRead() {
        // given
        Field field = new Field(1, "name1", FieldType.getByTypeCode(1), new Category(1));

        // when
        Field readField = fieldRepository.read(1);

        // then
        ReflectionAssert.assertReflectionEquals(field, readField);
    }

    @Test
    public void shouldReadByCategoryId() {
        // given
        List<Field> fields = new ArrayList<>();
        fields.add(new Field(1, "name1", FieldType.getByTypeCode(1), new Category(1)));
        fields.add(new Field(2, "name2", FieldType.getByTypeCode(0), new Category(1)));
        fields.add(new Field(3, "name3", FieldType.getByTypeCode(2), new Category(1)));

        // when
        List<Field> readFields = fieldRepository.readByCategoryId(1);

        // then
        ReflectionAssert.assertReflectionEquals(fields, readFields);
    }

    @Test
    public void shouldUpdate() {
        // given
        Field field = new Field(1, "new", FieldType.getByTypeCode(0), new Category(1));

        // when
        fieldRepository.update(new Field(1, "new", null, new Category(1)));
        Field readField = fieldRepository.read(1);

        // then
        Assert.assertEquals(field, readField);
    }

    @Test
    public void shouldDelete() {
        // when
        fieldRepository.delete(1);

        // then
        Assert.assertNull(fieldRepository.read(1));
    }
}
