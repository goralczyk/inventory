package goralczyk.inventory.repository;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.reflectionassert.ReflectionAssert;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class FieldValueRepositoryTest {

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @Test
    public void shouldBatchSave() {
        Map<Field, Object> attributes = new HashMap<>();
        attributes.put(new Field(1, "name1", FieldType.NUMBER, new Category(1)), 123);
        attributes.put(new Field(2, "name2", FieldType.STRING, new Category(1)), "str");
        attributes.put(new Field(3, "name3", FieldType.DATE, new Category(1)), new Date());

        fieldValueRepository.batchSave(2, attributes);
    }

    @Test
    public void shouldReadByItemId() {
        // given
        Map<Field, Object> attributes = new HashMap<>();
        attributes.put(new Field(1, "name1", FieldType.NUMBER, new Category(1)), 123);
        attributes.put(new Field(2, "name2", FieldType.STRING, new Category(1)), "str");
        attributes.put(new Field(3, "name3", FieldType.DATE, new Category(1)), new Date());
        fieldValueRepository.batchSave(2, attributes);

        // when
        Map<Field, Object> readAttributes = fieldValueRepository.readByItemId(2);

        // then
        ReflectionAssert.assertReflectionEquals(attributes, readAttributes, ReflectionComparatorMode.LENIENT_DATES);
    }

    @Test
    public void shouldBatchUpdate() {
        // given
        Map<Field, Object> attributes = new HashMap<>();
        attributes.put(new Field(1, "name1", FieldType.NUMBER, new Category(1)), 321);
        attributes.put(new Field(2, "name2", FieldType.STRING, new Category(1)), "rts");
        attributes.put(new Field(3, "name3", FieldType.DATE, new Category(1)), new Date());

        // when
        fieldValueRepository.batchUpdate(1, attributes);
        Map<Field, Object> readAttributes = fieldValueRepository.readByItemId(1);

        // then
        ReflectionAssert.assertReflectionEquals(attributes, readAttributes, ReflectionComparatorMode.LENIENT_DATES);
    }
}
