package goralczyk.inventory.repository;

import goralczyk.inventory.model.Category;
import goralczyk.inventory.model.Field;
import goralczyk.inventory.model.FieldType;
import goralczyk.inventory.model.Item;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.reflectionassert.ReflectionAssert;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import java.util.*;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class ItemRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void shouldSave() {
        Item item = new Item("name1", "desc1", 100, new Category(1));
        long id = itemRepository.save(item);

        Assert.assertEquals(6, id);
    }

    @Test
    public void shouldRead() {
        // given
        Item item = new Item(1, "name1", "desc1", 100, new Category(1), new Date());

        // when
        Item readItem = itemRepository.read(1);

        // then
        ReflectionAssert.assertReflectionEquals(item, readItem, ReflectionComparatorMode.LENIENT_DATES);
    }

    @Test
    public void shouldReadByCategoryId() {
        List<Item> readItems = itemRepository.readByCategoryId(1);
        Assert.assertEquals(2, readItems.size());
    }

    @Test
    public void shouldReadByCatalogId() {
        List<Item> readItems = itemRepository.readByCatalogId(1);
        Assert.assertEquals(5, readItems.size());
    }

    @Test
    public void shouldReadPaginatedByCatalogId() {
        // given
        for (int i = 0; i < 4; ++i) {
            itemRepository.save(new Item("name" + (i + 6), "", 1, new Category(2)));
        }

        // when
        List<Item> readItems = itemRepository.readPaginatedByCatalogId(1, 3, 3);

        // then
        for (int i = 0; i < 3; ++i) {
            Assert.assertEquals("name" + (i + 7), readItems.get(i).getName());
        }
    }

    @Test
    public void shouldReadPaginatedByCategory() {
        // given
        for (int i = 0; i < 7; ++i) {
            itemRepository.save(new Item("name" + (i + 3), "", 1, new Category(2)));
        }

        // when
        List<Item> readItems = itemRepository.readPaginatedByCategory(new Category(2), 3, 3);

        // then
        for (int i = 0; i < 3; ++i) {
            Assert.assertEquals("name" + (i + 7), readItems.get(i).getName());
        }
    }

    @Test
    public void shouldUpdate() {
        // given
        Item item = new Item(1, "name", "desc", 99, new Category(1), new Date());

        // when
        itemRepository.update(item);
        Item readItem = itemRepository.read(1);

        // then
        EqualsBuilder.reflectionEquals(item, readItem, "attributes");
    }

    @Test
    public void shouldDelete() {
        // when
        itemRepository.delete(1);

        // then
        Assert.assertNull(itemRepository.read(1));
    }

    @Test
    public void shouldCountByCategoryId() {
        Assert.assertEquals(5, itemRepository.countByCategoryId(1));
    }

    @Test
    public void shouldCountByCatalogId() {
        Assert.assertEquals(5, itemRepository.countByCatalogId(1));
    }
}
