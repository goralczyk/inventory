package goralczyk.inventory.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import goralczyk.inventory.model.Catalog;
import goralczyk.inventory.model.User;
import goralczyk.inventory.service.JsonService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class JsonServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CatalogRepository catalogRepository;

    @Autowired
    private JsonService jsonService;

    @Test
    public void shouldExportCatalogs() throws JsonProcessingException {
        List<Catalog> catalogs = catalogRepository.readByUserId(1);
        String json = jsonService.exportCatalogs(catalogs);
        //System.out.println(json);
    }

    @Test
    public void shouldImportCatalogs() throws IOException {
        // given
        String json1 = jsonService.exportCatalogs(catalogRepository.readByUserId(1));

        // when
        userRepository.save(new User(2, "", "", "", true));
        jsonService.importCatalogs(json1, 2);
        String json2 = jsonService.exportCatalogs(catalogRepository.readByUserId(2));

        // then
        Assert.assertEquals(json1, json2);
    }

    @Test
    public void shouldExportTreeView() throws JsonProcessingException {
        System.out.println(jsonService.exportCatalogTreeView(catalogRepository.read(1)));
    }

    @Test
    public void shouldExportRaw() throws JsonProcessingException {
        System.out.println(jsonService.exportCatalogsRaw(catalogRepository.readByUserId(1)));
    }
}
