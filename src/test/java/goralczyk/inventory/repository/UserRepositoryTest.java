package goralczyk.inventory.repository;

import goralczyk.inventory.model.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Tomasz Góralczyk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:db/h2/schema.sql", "classpath:db/h2/test-before.sql"})
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
        scripts = {"classpath:db/h2/test-after.sql"})
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldSave() {
        long id = userRepository.save(new User("name", "", "password", true));
        Assert.assertEquals(2, id);
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldNotSaveDuplicateEmail() {
        userRepository.save(new User("", "email", "", true));
    }

    @Test
    public void shouldReadByEmail() {
        User user = new User(1, "name", "email", "password", true);
        User readUser = userRepository.readByEmail(user.getEmail());

        Assert.assertTrue(EqualsBuilder.reflectionEquals(user, readUser));
    }

    @Test
    public void shouldUpdate() {
        User user = new User(1, "", "email", "", true);
        userRepository.update(user);
        User readUser = userRepository.read(user.getId());

        Assert.assertTrue(EqualsBuilder.reflectionEquals(user, readUser));
    }

    @Test
    public void shouldDelete() {
        userRepository.delete(1);
        Assert.assertNull(userRepository.read(1));
    }
}
