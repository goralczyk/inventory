INSERT INTO `User` (name, email, password, activated)
VALUES ('name', 'email', 'password', 'true');

-- catalogs
INSERT INTO Catalog (name, tag, description, user_id)
VALUES ('name1', 'tag1', 'desc1', '1');
INSERT INTO Catalog (name, tag, description, user_id)
VALUES ('name2', 'tag2', 'desc2', '1');
INSERT INTO Catalog (name, tag, description, user_id)
VALUES ('name3', 'tag3', 'desc3', '1');

-- categories
INSERT INTO Category (name, tag, description, catalog_id, parent_id)
VALUES ('name1', 'tag1', 'desc1', 1, NULL);
INSERT INTO Category (name, tag, description, catalog_id, parent_id)
VALUES ('name2', 'tag2', 'desc2', 1, 1);
INSERT INTO Category (name, tag, description, catalog_id, parent_id)
VALUES ('name3', 'tag3', 'desc3', 1, 2);
INSERT INTO Category (name, tag, description, catalog_id, parent_id)
VALUES ('name4', 'tag4', 'desc4', 1, NULL);
INSERT INTO Category (name, tag, description, catalog_id, parent_id)
VALUES ('name5', 'tag5', 'desc5', 1, 1);

-- fields
INSERT INTO Field (type, name, category_id)
VALUES (1, 'name1', 1);
INSERT INTO Field (type, name, category_id)
VALUES (0, 'name2', 1);
INSERT INTO Field (type, name, category_id)
VALUES (2, 'name3', 1);

-- items
INSERT INTO Item (name, description, quantity, category_id)
VALUES ('name1', 'desc1', 100, 1);
INSERT INTO Item (name, description, quantity, category_id)
VALUES ('name2', 'desc2', 100, 1);
INSERT INTO Item (name, description, quantity, category_id)
VALUES ('name3', 'desc3', 100, 2);
INSERT INTO Item (name, description, quantity, category_id)
VALUES ('name4', 'desc4', 100, 3);
INSERT INTO Item (name, description, quantity, category_id)
VALUES ('name5', 'desc5', 100, 5);

-- values
INSERT INTO FieldValueNumber (value, item_id, field_id)
VALUES (123, 1, 1);
INSERT INTO FieldValueString (value, item_id, field_id)
VALUES ('str', 1, 2);
INSERT INTO FieldValueDate (value, item_id, field_id)
VALUES (CURRENT_DATE, 1, 3);